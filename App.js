import React from 'react';
import { Font } from 'expo';
import { ScreenOrientation } from 'expo';
import { Provider } from "react-redux";
import store from "./config/store";
import AppContainer from './config/navigation';
import { View } from 'react-native';
export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  componentDidMount() {
    ScreenOrientation.allowAsync(ScreenOrientation.Orientation.LANDSCAPE);
    this.loadFonts();
  }

  async loadFonts() {
    await Font.loadAsync({
      'titillium': require('./assets/fonts/titillium/TitilliumWeb-Regular.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  render() {

    if (!this.state.fontLoaded)
      return (<View></View>)

    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
