import React, { Component } from 'react'
import { Text, View, StyleSheet, TouchableHighlight, TextInput, ActivityIndicator } from 'react-native';
import { login } from './redux/account.action';
import { connect } from "react-redux";
import { BACKGROUND_COLOR } from '../../config/colors';

export class LoginComponent extends Component {

    constructor(props) {
        super(props);
        this.state = { email: 'test@test.com', password: 'test', submitted: false }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.logued) {
            this.navigate();
        }
    }

    navigate = () => {
        this.props.navigation.navigate('Match');
    }

    login = () => {
        this.setState({ submitted: true })
        this.props.login({ email: this.state.email, password: this.state.password })
    }

    render() {
        const { submitted } = this.state;
        const { login_error, loading } = this.props;

        return (
            <View style={styles.container}>
                <View style={styles.formContainer}>
                    <Text style={styles.text}>USERNAME</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(email) => this.setState({ email })}
                        value={this.state.email}
                    />
                </View>
                <View style={styles.formContainer}>
                    <Text style={styles.text}>PASSWORD</Text>
                    <TextInput style={styles.input}
                        onChangeText={(password) => this.setState({ password })}
                        value={this.state.password}
                    />
                </View>

                {submitted && login_error && (
                    <View>
                        <Text style={styles.textError}>{login_error.reason}</Text>
                    </View>
                )}

                <TouchableHighlight
                    style={styles.buttonContainer}
                    onPress={this.login} >
                    {submitted && !login_error ?
                        (<ActivityIndicator size="small" color="#fff" />)
                        :
                        <Text style={styles.buttonText}> Sign In </Text>
                    }
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 24,
        flex: 1,
        backgroundColor: BACKGROUND_COLOR
    },
    formContainer: {
        height: 60,
        width: 350,
        marginTop: 5,
        flexDirection: 'row',
        alignItems: 'center'
    },
    text: {
        flex: 1,
        color: "#fff",
        fontFamily: 'titillium'
    },
    input: {
        flex: 3,
        borderColor: "#fff",
        backgroundColor: "#fff",
        padding: 5,
    },
    buttonContainer: {
        marginTop: 20,
        width: 250,
        height: 50,
        backgroundColor: "#0b6074",
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: '#fff',
        fontSize: 15,
        fontFamily: 'titillium'
    },
    textError: {
        color: '#f00'
    }
});

const mapStateToProps = (state) => ({
    ...state.account
})

const mapDispatchToProps = {
    login
}

const LoginScene = connect(mapStateToProps, mapDispatchToProps)(LoginComponent)
export default LoginScene;

