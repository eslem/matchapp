import {
    takeEvery, call, put, all
} from "redux-saga/effects";
import { LOGIN, LOGIN_LOADED, LOGIN_ERROR } from './account.types';
import { login } from './account.api'

export function* loginSaga() {
    yield takeEvery(LOGIN, loginAsync)
}

function* loginAsync(action) {
    try {
        const payload = yield call(login, action.payload)
        if (payload.ok)
            yield put({
                type: LOGIN_LOADED
            })
        else {
            yield put({
                type: LOGIN_ERROR,
                payload: { reason: "No valid user or password" }
            })
        }
    } catch (e) {
        yield put({
            type: LOGIN_ERROR,
            payload: e
        })
    }
}


export default function* accountSaga() {
    yield all([
        loginSaga()
    ])
}