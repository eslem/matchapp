import { LOGIN } from "./account.types";

export const login = (payload) => ({
    type: LOGIN,
    payload
})
