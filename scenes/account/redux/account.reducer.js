import { LOGIN_LOADED, LOGIN_ERROR } from './account.types'

const initialState = {
    user: null,
    login_error: null
}

export default accountReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case LOGIN_LOADED:
            return { ...state, logued: true };
        case LOGIN_ERROR:
            return { ...state, login_error: payload }
        default:
            return state
    }
}
