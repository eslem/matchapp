import { BASE_URL } from "../../../config/constants";

export const login = (params) => {
    const url = BASE_URL + '/login';
    const config = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(params)
    }
    return fetch(url, config);
}
