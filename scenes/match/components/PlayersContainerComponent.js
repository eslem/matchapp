import * as React from 'react';
import { Text, View, StyleSheet, TouchableHighlight, Button } from 'react-native';
import { TEAM_A, TEAM_B } from '../../../config/colors';
import PropTypes from 'prop-types';
import PlayerItemComponent from './PlayerItemComponent';

const colors = {
  A: TEAM_A,
  B: TEAM_B
}

const PlayersContainerComponent = ({ team, type, selected, playerClick }) => {
  if (!team)
    return <View></View>
  return (
    <View style={styles.container}>
      <View style={[styles.title, { backgroundColor: colors[type] }]}>
        <Text style={styles.titleText}>TEAM {type}</Text>
      </View>
      {
        team.players.map(el => <PlayerItemComponent key={el.id} item={el}
          type={type}
          selected={selected}
          onPlayerClick={item => playerClick({ team: type, player: item })} />)
      }
    </View>
  );
}

PlayersContainerComponent.propTypes = {
  team: PropTypes.object,
  type: PropTypes.string,
  selected: PropTypes.object,
  playerClick: PropTypes.func
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    marginRight: 5
  },
  title: {
    height: 50,
    backgroundColor: '#FF8735',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'titillium'
  },
  titleText: {
    fontSize: 10,
    color: '#fff',
    fontFamily: 'titillium'
  }
});

export default PlayersContainerComponent;