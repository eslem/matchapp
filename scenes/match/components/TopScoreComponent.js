import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput, Button } from 'react-native';
import { SCORE } from '../../../config/colors';
import PropTypes from 'prop-types';
import TeamScoreComponent from './TeamScoreComponent';

function formatTime(time) {
  let minutes = Math.floor(time / 60);
  time -= minutes * 60;
  let seconds = parseInt(time % 60, 10);
  return `${minutes < 10 ? `0${minutes}` : minutes}:${seconds < 10 ? `0${seconds}` : seconds}`;
}

const TopScoreComponent = ({ teams, period, time, score }) => {
  if (!teams)
    return <View />;

  return (
    <View style={styles.container}>
      <TeamScoreComponent team={teams["teamA"]} score={score["teamA"]} />
      <View style={styles.line} />
      <View style={styles.teamContainer}>
        <Text style={styles.teamTitle}> Period {period} </Text>
        <Text style={styles.teamScore}>{formatTime(time)}</Text>
      </View>
      <View style={styles.line} />
      <TeamScoreComponent team={teams["teamB"]} score={score["teamB"]} />
    </View >
  );
}

TopScoreComponent.propTypes = {
  period: PropTypes.number,
  time: PropTypes.number,
  teams: PropTypes.object
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 2,
    borderColor: '#fff'
  },
  teamContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  teamTitle: {
    flex: 1,
    fontSize: 18,
    color: '#fff',
    fontFamily: 'titillium'
  },
  line: {
    width: 2,
    backgroundColor: '#fff',
    marginTop: 20,
    marginBottom: 20,
  },
  teamScore: {
    flex: 2,
    fontSize: 25,
    color: SCORE
  }
});

export default TopScoreComponent;