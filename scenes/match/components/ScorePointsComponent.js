import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput, Button, FlatList } from 'react-native';
import PropTypes from 'prop-types';
import ScoreRowComponent from './ScoreRowComponent';

//Class because needs update for actions
export default class ScorePointsComponent extends React.Component {
  render() {
    const { actions } = this.props;
    if (actions.length > 0)
      return (
        <View style={styles.container}>
          <FlatList
            data={actions}
            renderItem={({ item }) => <ScoreRowComponent item={item} />}
            keyExtractor={(item) => item.time}
            extraData={this.props}
          />
        </View>
      );
    else {
      return (
        <View style={styles.container}>
          <Text style={styles.textItem}>No points yet</Text>
        </View>
      )
    }
  }
}

ScorePointsComponent.propTypes = {
  actions: PropTypes.array
};

const styles = StyleSheet.create({
  container: {
    flex: 4,
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 2,
    borderColor: '#fff',
    backgroundColor: '#010101'
  }
});