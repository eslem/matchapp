import React from 'react'
import PropTypes from 'prop-types'
import { Text, StyleSheet, TouchableHighlight } from 'react-native';
import { POINT, POINT_ACTIVE } from '../../../config/colors';

const PointButtonsComponent = ({ selectedPoint, point, pointClick }) => {
    var styleActive = {};
    if (selectedPoint === point) {
        styleActive = { backgroundColor: POINT_ACTIVE }
    }
    return (
        <TouchableHighlight
            style={[styles.scoreButton, styleActive]}
            onPress={() => pointClick(point)} >
            <Text style={styles.textScore}>{point} POINTS</Text>
        </TouchableHighlight>
    )
}

PointButtonsComponent.propTypes = {
    selectedPoint: PropTypes.number,
    point: PropTypes.number,
    pointClick:PropTypes.func
}

const styles = StyleSheet.create({
    scoreButton: {
        backgroundColor: POINT,
        flex: 1,
        marginBottom: 5,
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
        flexDirection: 'column'
    },
    textScore: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'titillium'
    }
});

export default PointButtonsComponent
