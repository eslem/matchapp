import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { TEAM_A, TEAM_B } from '../../../config/colors';

const ScoreRowComponent = ({ item }) => {
    var typeStyle = {
        color: TEAM_A
    }

    if (item.team === "B") {
        typeStyle.color = TEAM_B
    }

    return (
        <View style={styles.item}>
            <Text style={[styles.textItem, typeStyle]}>{item.team}</Text>
            <Text style={styles.textItem}>{item.score}</Text>
            <Text style={styles.textItem}>{item.points} POINTS</Text>
        </View>
    )
}

ScoreRowComponent.propTypes = {
    item: PropTypes.object
}

const styles = StyleSheet.create({
    item: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10
    },
    textItem: {
        flex: 1,
        textAlign: 'center',
        color: '#fff',
        fontFamily: 'titillium'
    }
});

export default ScoreRowComponent;