import * as React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import TimerButtonComponent from './TimerButtonComponent';
import PointButtonsComponent from './PointButtonsComponent';

const ButtonsContainerComponent = ({ selectedPoint, pointClick, startTimer, stopTimer, isPlaying }) => {
  return (
    <View style={styles.container}>
      <View style={styles.scoreButtons}>
        <PointButtonsComponent point={2} {... { selectedPoint, pointClick }} />
        <PointButtonsComponent point={3} {... { selectedPoint, pointClick }} />
      </View>
      <TimerButtonComponent {... { startTimer, stopTimer, isPlaying }} />
    </View>
  );
};

ButtonsContainerComponent.propTypes = {
  selectedPoint: PropTypes.number,
  pointClick: PropTypes.func,
  startTimer: PropTypes.func,
  stopTimer: PropTypes.func,
  poinisPlayingtClick: PropTypes.bool
}

const styles = StyleSheet.create({
  container: {
    flex: 4,
    justifyContent: 'space-between'
  },
  scoreButtons: {
    height: 100,
    flexDirection: 'column'
  }
});

export default ButtonsContainerComponent;