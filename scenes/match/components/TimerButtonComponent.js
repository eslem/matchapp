import * as React from 'react';
import { View, StyleSheet, TouchableHighlight } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import PropTypes from 'prop-types';

function TimerButtonComponent({ isPlaying, startTimer, stopTimer }) {
    return (
        <View style={styles.endButtonContainer}>
            {!isPlaying &&
                (<TouchableHighlight style={styles.endButton} onPress={startTimer}>
                    <FontAwesome name="play" size={15} color="white" />
                </TouchableHighlight>)
            }
            {isPlaying &&
                (<TouchableHighlight style={styles.endButton} onPress={stopTimer}>
                    <FontAwesome name="stop" size={15} color="white" />
                </TouchableHighlight>)
            }
        </View>
    )
}

TimerButtonComponent.propTypes = {
    isPlaying: PropTypes.bool,
    startTimer: PropTypes.func,
    stopTimer: PropTypes.func,
};


const styles = StyleSheet.create({
    endButtonContainer: {
        height: 50
    },
    endButton: {
        backgroundColor: '#d9d9d9',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default TimerButtonComponent;