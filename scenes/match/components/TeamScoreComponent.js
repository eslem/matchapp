import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, StyleSheet } from 'react-native';
import { SCORE } from '../../../config/colors';

const TeamScoreComponent = ({ team, score }) => {
  return (
    <View style={styles.teamContainer}>
      <Text style={styles.teamTitle}>{team.name}</Text>
      <Text style={styles.teamScore}>{score}</Text>
    </View>
  )
}

TeamScoreComponent.propTypes = {
  team: PropTypes.object,
  score: PropTypes.number
}

const styles = StyleSheet.create({
  teamContainer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  teamTitle: {
    flex: 1,
    fontSize: 18,
    color: '#fff',
    fontFamily: 'titillium'
  },
  teamScore: {
    flex: 2,
    fontSize: 25,
    color: SCORE
  }
});

export default TeamScoreComponent;