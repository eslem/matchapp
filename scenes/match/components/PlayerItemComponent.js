import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, StyleSheet, TouchableHighlight, Button } from 'react-native';
import { TEAM_A, TEAM_B } from '../../../config/colors';

const colors = {
    A: TEAM_A,
    B: TEAM_B
}

function isActive(element, selected, type) {
    if (!selected || !selected.team)
        return false;
    return selected.team === type && selected.player.id == element.id;
}

const PlayerItemComponent = ({ item, selected, type, onPlayerClick }) => {
    var active = { backgroundColor: colors[type] };
    if (isActive(item, selected, type)) {
        active = { backgroundColor: '#f00' }
    }
    return (
        <TouchableHighlight style={[styles.player, active]} onPress={() => onPlayerClick(item)}>
            <Text style={styles.textPlayer}>{item.number}</Text>
        </TouchableHighlight>
    )
}

PlayerItemComponent.propTypes = {
    item: PropTypes.object,
    type: PropTypes.string,
    selected: PropTypes.object,
    onPlayerClick: PropTypes.func
}

const styles = StyleSheet.create({
    player: {
        height: 60,
        marginTop: 5,
        backgroundColor: "#FF8735",
        alignItems: 'center',
        justifyContent: 'center',
    },
    textPlayer: {
        fontSize: 30,
        color: '#fff',
        fontFamily: 'titillium'
    }
});

export default PlayerItemComponent;