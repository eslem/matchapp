import * as React from 'react';
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { loadMatch, setPlayer, setPoint, startTimer, addSecond, stopTimer, postActions } from './redux/match.action'
import { connect } from "react-redux";

import { BACKGROUND_COLOR } from '../../config/colors';

import TopScoreComponent from './components/TopScoreComponent';
import ScorePointsComponent from './components/ScorePointsComponent';
import PlayersContainerComponent from './components/PlayersContainerComponent';
import ButtonsContainerComponent from './components/ButtonsContainerComponent';

export class MatchComponent extends React.Component {

  componentWillMount() {
    this.props.loadMatch();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const currentProps = this.props;
    if (!currentProps.isPlaying && nextProps.isPlaying) {
      const timerInterval = setInterval(() => {
        currentProps.addSecond();
      }, 1000);
      this.setState({ timerInterval })
    } else if (currentProps.isPlaying && !nextProps.isPlaying) {
      clearInterval(this.state.timerInterval)
    }
  }


  render() {
    const { match, score, period, selected, selectedPoint, actions, elapsedTime, timerDuration, isPlaying, match_error } = this.props;
    const { setPoint, setPlayer, startTimer, stopTimer } = this.props;
    if (!match && !match_error)
      return (<View style={styles.container}><ActivityIndicator size="large" color="#fff" /></View>);

    if (!match && match_error)
      return (<View style={styles.container}><Text style={styles.error}>{match_error}</Text></View>);


    return (
      <View style={styles.container}>
        <View style={styles.playersContainer}>
          <PlayersContainerComponent playerClick={setPlayer}
            team={match.teamA}
            selected={selected}
            type="A" />
          <PlayersContainerComponent
            playerClick={setPlayer}
            team={match.teamB}
            selected={selected}
            type="B" />
        </View>
        <View style={styles.scoreContainer}>
          <TopScoreComponent teams={match} score={score} period={period}
            time={(timerDuration - elapsedTime)} />
          <View style={styles.topScore}></View>
          <ScorePointsComponent actions={actions} />
        </View>
        <View style={styles.pointsContainer}>
          <View style={styles.placeHolder}></View>
          <ButtonsContainerComponent
            pointClick={setPoint}
            selectedPoint={selectedPoint}
            startTimer={startTimer}
            stopTimer={stopTimer}
            isPlaying={isPlaying}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    padding: 5,
    flex: 1,
    flexDirection: 'row',
    marginTop: 15,
    backgroundColor: BACKGROUND_COLOR
  },
  playersContainer: {
    flex: 1,
    paddingLeft: 5,
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  scoreContainer: {
    flex: 3,
    paddingTop: 5,
  },
  pointsContainer: {
    flex: 1,
    padding: 5,
  },
  placeHolder: {
    flex: 1
  },
  topScore: {
    height: 30,
    marginTop: 5,
    backgroundColor: '#fff'
  },
  error: {
    color: "#f00",
    marginTop: 20
  }
});


const mapStateToProps = (state) => ({
  ...state.match
})

const mapDispatchToProps = {
  loadMatch, setPlayer, setPoint, startTimer, addSecond, stopTimer
}

const MatchSCene = connect(mapStateToProps, mapDispatchToProps)(MatchComponent)
export default MatchSCene;

