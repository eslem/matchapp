import { BASE_URL } from "../../../config/constants";

export const getMatch = (params) => {
    const url = BASE_URL + '/match';
    return fetch(url).then(res => res.json());
}

export const uploadActions = (params) => {
    const url = BASE_URL + '/match';
    const config = {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(params)
    }
    return fetch(url, config);
}
