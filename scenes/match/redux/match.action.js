export const LOAD_MATCH = "LOAD_MATCH"
export const MATCH_LOADED = "MATCH_LOADED"
export const MATCH_LOAD_ERROR = "MATCH_LOAD_ERROR"
export const SELECT_PLAYER = "SELECT_PLAYER"
export const SELECT_POINT = "SELECT_POINT"
export const START_TIMER = "START_TIMES";
export const STOP_TIMER = "STOP_TIMER";
export const ADD_SECOND = "ADD_SECOND";
export const POST_ACTIONS = "POST_ACTIONS";


export const loadMatch = (payload) => ({
  type: LOAD_MATCH,
  payload
})

export const setPlayer = (payload) => ({
  type: SELECT_PLAYER,
  payload
})

export const setPoint = (payload) => ({
  type: SELECT_POINT,
  payload
})


export const startTimer = () => ({
  type: START_TIMER
})

export const addSecond = () => ({
  type: ADD_SECOND
})


export const stopTimer = () => ({
  type: STOP_TIMER
})

export const postActions = () => ({
  type: POST_ACTIONS
})
