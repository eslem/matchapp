import { MATCH_LOADED, MATCH_LOAD_ERROR, SELECT_PLAYER, SELECT_POINT, ADD_SECOND, START_TIMER, STOP_TIMER, POST_ACTIONS } from './match.action'
import { ToastAndroid } from 'react-native';
import { uploadActions } from './match.api';

const TIMER_DURATION = 10 * 60;
const initialState = {
    match: null,
    actions: [],
    match_error: null,
    score: {
        teamA: 0,
        teamB: 0
    },
    period: 0,
    actionId: 0,
    selected: {
        team: null,
        player: null
    },
    isPlaying: false,
    elapsedTime: 0,
    selectedPoint: null,
    timerDuration: TIMER_DURATION
}

var num = 0;


function applyStartTimer(state) {
    return {
        ...state,
        isPlaying: true,
        period: state.period + 1
    }
}

function applyStopTimer(state) {
    postActions(state);
    return {
        ...state,
        isPlaying: false,
        elapsedTime: 0
    }
}

function applyAddSecond(state) {
    if (state.elapsedTime < TIMER_DURATION) {
        const elapsedTime = state.elapsedTime + 1;

        //Every 10 seconds
        if (elapsedTime % 10 === 0) {
            postActions(state);
        }

        return {
            ...state,
            elapsedTime
        }
    } else {
        postActions(state);
        return {
            ...state,
            isPlaying: false
        }
    }
}


function postActions(state) {
    const body = [];
    for (var i = 0; i < state.actions.length; i++) {
        const action = state.actions[i];
        //actions are unshift so when an action is uploaded i can break the form
        if (action.uploaded)
            break;
        body.push({
            id: action.id,
            playerId: action.playerId,
            value: action.points,
            type: "POINTS"
        });
        action.uploaded = true;
    }
    if (body.length > 0) {
        uploadActions(body).then(() => console.log('uploaded'));
    }
    return state;
}


function checkAction(state) {
    if (!state.isPlaying) {
        ToastAndroid.show('Period not started', ToastAndroid.SHORT);
        clearSelection(state);
    }

    if (state.isPlaying && state.selectedPoint && state.selected.player) {
        state.actions.unshift(createAction(state));
        clearSelection(state);
    }
}

function createAction(state) {
    addPoints(state, state.selectedPoint, "team" + state.selected.team);
    const action = {
        id: state.actionId++,
        team: state.selected.team,
        points: state.selectedPoint,
        score: state.score.teamA + "-" + state.score.teamB,
        playerId: state.selected.player.id,
        time: new Date().toISOString(),
        uploaded: false
    }
    return action;
}

function clearSelection(state) {
    state.selectedPoint = null;
    state.selected = {
        team: null,
        player: null
    }
}

function addPoints(state, points, team) {
    state.score[team] += points;
}

function applySelectPlayer(state, payload) {
    var newState = { ...state, selected: payload }
    checkAction(newState);
    return newState
}

function applySelectPoint(state, payload) {
    var newState = { ...state, selectedPoint: payload }

    checkAction(newState);
    return newState
}



export default matchReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case MATCH_LOADED:
            return { ...state, match: payload };
        case MATCH_LOAD_ERROR:
            return { ...state, match_error: payload }
        case SELECT_PLAYER:
            return applySelectPlayer(state, payload);
        case SELECT_POINT:
            return applySelectPoint(state, payload);
        case START_TIMER:
            return applyStartTimer(state);
        case ADD_SECOND:
            return applyAddSecond(state);
        case STOP_TIMER:
            return applyStopTimer(state);
        case POST_ACTIONS:
            return postActions(state);
        default:
            return state
    }
}
