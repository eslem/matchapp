import {
    takeEvery,
    call,
    put,
    all
} from "redux-saga/effects";
import { LOAD_MATCH, MATCH_LOADED, MATCH_LOAD_ERROR } from './match.action';
import { getMatch } from './match.api'

export function* loadMatchSaga() {
    yield takeEvery(LOAD_MATCH, loadMatchAsync)
}

function* loadMatchAsync(action) {
    try {
        const payload = yield call(getMatch)
        yield put({
            type: MATCH_LOADED,
            payload
        })

    } catch (e) {
        yield put({
            type: MATCH_LOAD_ERROR,
            payload: "Cannot load match"
        })
    }
}


export default function* matchSaga() {
    yield all([
        loadMatchSaga()
    ])
}