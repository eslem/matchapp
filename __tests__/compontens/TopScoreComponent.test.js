import React from 'react';
import renderer from 'react-test-renderer';

import TeamScoreComponent from './../../scenes/match/components/TeamScoreComponent';

describe('<TeamScoreComponent />', () => {
    it('has 1 child', () => {
        const tree = renderer.create(<TeamScoreComponent team={{name:"A"}} score={2} />).toJSON();
        expect(tree.children.length).toBe(2);
        expect(tree).toMatchSnapshot();
    });

    it('chould change the content', () => {
        const tree = renderer.create(<TeamScoreComponent team={{name:"A"}} score={4} />).toJSON();
        expect(tree.children.length).toBe(2);
        expect(tree).toMatchSnapshot();
    });
});