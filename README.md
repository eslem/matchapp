# NBN23 Match App

## Stack
* React Native
* Expo
* React-Redux
* React-Saga
* Gitlab CI

## Folder Structure
```
  config/
    |──navigation.js
    |──store.js
    |──colors.js
    |──constants.js
    |──scenes
        |──account
            |── ...
        |──match
            |──redux
            |──components
            MatchScene.js
  App.js
  package.json
  ```


### Login Scene
- Form with action to login making a post to the server ( [ExpressJS server](https://gitlab.com/eslem/matchserver) ),
- Spinner to show the loading state
- Navigate to MatchScene is login correct, if not show error

![Login Scene](assets/screenshots/login.jpg)


### Match Scene
- Data of match loaded from server
- Countdown buttons
- Tap player and score to add action
    - only* if the match start, if not will show an error
    - active states for visual feedback of player and points
- Score total & countdown
- Team information & player selection
- Scene will call the api to post the actions every 10 seconds


![Login Scene](assets/screenshots/match.jpg)

<!-- 
## CI
- test on push with jest
- deploy and build with expo -->