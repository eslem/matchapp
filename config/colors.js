export const BACKGROUND_COLOR = "#001027";
export const TEAM_A = "#ec5400";
export const TEAM_B = "#ffe55c";

export const SCORE = "#edce42";

export const POINT = "#458d44";
export const POINT_ACTIVE = "#62c75d";