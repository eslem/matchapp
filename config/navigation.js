import LoginScene from '../scenes/account/LoginScene';
import { createStackNavigator, createAppContainer } from "react-navigation";
import MatchSCene from '../scenes/match/MatchScene';

const AppNavigator = createStackNavigator(
    {
        Login: LoginScene,
        Match: MatchSCene
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none'
    }
)

export default createAppContainer(AppNavigator);