import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers

} from "redux";
import { all } from 'redux-saga/effects';
import createSagaMiddleware from "redux-saga";

import accountReducer from '../scenes/account/redux/account.reducer';
import accountSaga from '../scenes/account/redux/account.saga';
import matchSaga from "../scenes/match/redux/match.saga";
import matchReducer from "../scenes/match/redux/match.reducer";

function* rootSaga() {
    yield all([
        accountSaga(),
        matchSaga()
    ])
}
const initialiseSagaMiddleware = createSagaMiddleware();

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    combineReducers({
        account: accountReducer,
        match: matchReducer
    }),
    storeEnhancers(applyMiddleware(initialiseSagaMiddleware))
);

initialiseSagaMiddleware.run(rootSaga);

export default store;